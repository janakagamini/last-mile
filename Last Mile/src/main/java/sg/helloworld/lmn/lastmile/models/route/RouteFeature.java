package sg.helloworld.lmn.lastmile.models.route;

/**
 * Created by Janaka on 4/13/2014.
 */
public class RouteFeature {
    private String type;
    private int id;
    private FeatureGeometry geometry;
    private FeatureProperty properties;

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public FeatureGeometry getGeometry() {
        return geometry;
    }

    public FeatureProperty getProperties() {
        return properties;
    }
}
