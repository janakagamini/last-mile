package sg.helloworld.lmn.lastmile.providers;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;

import sg.helloworld.lmn.lastmile.models.poi.PointOfInterest;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterestWrapper;
import sg.helloworld.lmn.lastmile.utils.LmnApiWrapper;

public class LmnProvider extends ContentProvider {

    public final static String URI = "content://sg.helloworld.lmn.lastmile.providers.lmnProvider";
    private final static String TAG = LmnProvider.class.getName();
    private static final String[] SEARCH_SUGGEST_COLUMNS = {
            BaseColumns._ID,
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA/*,
            SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA*/
    };
    LmnApiWrapper lmnService;

    public LmnProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        lmnService = LmnApiWrapper.getInstance();
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        MatrixCursor nCursor = new MatrixCursor(SEARCH_SUGGEST_COLUMNS);

        // TODO: set mapName dynamically
        String mapName = "sggbtb";
        String poiName = selectionArgs[0];
        PointOfInterestWrapper pois = lmnService.getPoi(mapName, poiName);

        for (PointOfInterest poi : pois.getPois()) {
            nCursor.addRow(new Object[]{poi.getPid(), poi.getName(), poi.getName()});
        }

        return nCursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
