package sg.helloworld.lmn.lastmile.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import sg.helloworld.lmn.lastmile.R;


public class MainActivity extends Activity {

    //LmnApiWrapper lmnService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*lmnService = LmnApiWrapper.getInstance();

        lmnService.getRoute("sgzoo", 1, 42, new Callback<RouteWrapper>() {
            @Override
            public void success(RouteWrapper routeWrapper, Response response) {
                Log.d("DEBUG", "Retrieved route, has " + routeWrapper.getFeatures().size() + " points");
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

            }
        });*/

        Button button = (Button) findViewById(R.id.localMaps);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LocalTileActivity.class);
                startActivity(intent);
            }
        });

        button = (Button) findViewById(R.id.onlineMaps);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, OnlineTileActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
