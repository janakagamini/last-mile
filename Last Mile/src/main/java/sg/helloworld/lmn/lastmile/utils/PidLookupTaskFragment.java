package sg.helloworld.lmn.lastmile.utils;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import sg.helloworld.lmn.lastmile.main.LocalTileActivity;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterestWrapper;

/**
 * Created by janaka on 4/17/14.
 */
public class PidLookupTaskFragment extends Fragment {

    private PidLookUpCallbacks callbacks;
    private PidLookUpTask task;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (PidLookUpCallbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Bundle bundle = getArguments();
        String mapName = bundle.getString(LocalTileActivity.MAP_NAME);
        int taskType = bundle.getInt(LocalTileActivity.PID_TASK_TYPE);
        String[] poiNames = bundle.getStringArray(LocalTileActivity.POI_NAMES);
        task = new PidLookUpTask(mapName, taskType, poiNames);
        task.execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    public interface PidLookUpCallbacks {
        void onPreExecute();

        void onProgressUpdate();

        void onCancelled(String reason);

        void onPostExecute(int taskType, String[] poiNames, List<Integer[]> pidList);
    }

    private class PidLookUpTask extends AsyncTask<Void, Void, List<Integer[]>> {

        private LmnApiWrapper lmnService;
        private String mapName;
        private int taskType;
        private String[] poiNames;
        private String reason;

        public PidLookUpTask(String mapName, int taskType, String[] poiNames) {
            lmnService = LmnApiWrapper.getInstance();
            this.mapName = mapName;
            this.taskType = taskType;
            this.poiNames = poiNames;
            reason = "Could not find a route.";
        }

        @Override
        protected List<Integer[]> doInBackground(Void... voids) {

            List<Integer[]> pidList = new ArrayList<Integer[]>(poiNames.length);

            for (String poiName : poiNames) {
                try {
                    PointOfInterestWrapper pointOfInterestWrapper = lmnService.getPoi(mapName, poiName);

                    if (pointOfInterestWrapper.getPois().size() > 0) {
                        Integer[] pids = new Integer[pointOfInterestWrapper.getPois().size()];
                        for (int i = 0; i < pointOfInterestWrapper.getPois().size(); i++) {
                            pids[i] = pointOfInterestWrapper.getPois().get(i).getPid();
                        }
                        pidList.add(pids);
                    } else {
                        reason = "No results for " + poiName;
                        cancel(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    reason = "Could not reach LMN service, please check your internet connection.";
                    cancel(true);
                }
            }

            return pidList;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(List<Integer[]> pidList) {
            callbacks.onPostExecute(taskType, poiNames, pidList);
        }

        @Override
        protected void onCancelled() {
            callbacks.onCancelled(reason);
        }
    }
}
