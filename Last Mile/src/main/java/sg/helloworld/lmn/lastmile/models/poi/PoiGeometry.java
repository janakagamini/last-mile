package sg.helloworld.lmn.lastmile.models.poi;

/**
 * Created by Janaka on 4/13/2014.
 */
public class PoiGeometry {
    private String type;
    private double[] coordinates;

    public String getType() {
        return type;
    }

    public double[] getCoordinates() {
        return coordinates;
    }
}
