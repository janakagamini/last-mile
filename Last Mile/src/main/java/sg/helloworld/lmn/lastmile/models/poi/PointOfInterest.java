package sg.helloworld.lmn.lastmile.models.poi;

/**
 * Created by Janaka on 4/13/2014.
 */
public class PointOfInterest {

    private String name;
    private int pid;
    private PoiGeometry geometry;

    public String getName() {
        return name;
    }

    public int getPid() {
        return pid;
    }

    public PoiGeometry getGeometry() {
        return geometry;
    }
}
