package sg.helloworld.lmn.lastmile.utils;

import retrofit.Callback;
import retrofit.RestAdapter;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterestWrapper;
import sg.helloworld.lmn.lastmile.models.route.RouteWrapper;

/**
 * Created by Janaka on 14/4/2014.
 */
public class LmnApiWrapper {

    private static final String BASE_URL = "http://lmn.helloworld.sg:3000";
    private static final String GET_POI = ".poi.get";
    private static final String GET_ROUTE = ".route.get";
    private static LmnApiWrapper instance = null;
    LmnApi lmnApi;

    public LmnApiWrapper() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        lmnApi = restAdapter.create(LmnApi.class);
    }

    public synchronized static LmnApiWrapper getInstance() {
        if (instance == null) {
            instance = new LmnApiWrapper();
        }

        return instance;
    }

    public void getPoi(String mapName, String poiName, Callback<PointOfInterestWrapper> cb) {
        lmnApi.getPoi(mapName + GET_POI, poiName, cb);
    }

    public PointOfInterestWrapper getPoi(String mapName, String poiName) {
        return lmnApi.getPoi(mapName + GET_POI, poiName);
    }

    public void getRoute(String mapName, int source, int target, Callback<RouteWrapper> cb) {
        lmnApi.getRoute(mapName + GET_ROUTE, source, target, cb);
    }
}
