package sg.helloworld.lmn.lastmile.models.poi;

import java.util.List;

/**
 * Created by Janaka on 4/13/2014.
 */
public class PointOfInterestWrapper {

    private List<PointOfInterest> pois;

    public List<PointOfInterest> getPois() {
        return pois;
    }
}
