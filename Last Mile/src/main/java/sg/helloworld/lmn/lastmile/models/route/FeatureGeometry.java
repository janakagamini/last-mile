package sg.helloworld.lmn.lastmile.models.route;

import java.util.List;

/**
 * Created by Janaka on 14/4/2014.
 */
public class FeatureGeometry {
    private String type;
    private List<double[]> coordinates;

    public String getType() {
        return type;
    }

    public List<double[]> getCoordinates() {
        return coordinates;
    }
}
