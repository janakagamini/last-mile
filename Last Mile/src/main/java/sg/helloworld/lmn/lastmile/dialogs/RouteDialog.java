package sg.helloworld.lmn.lastmile.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;

import sg.helloworld.lmn.lastmile.R;
import sg.helloworld.lmn.lastmile.providers.LmnProvider;

/**
 * Created by Janaka on 16/4/2014.
 */
public class RouteDialog extends DialogFragment {

    //Debug TAG
    private static final String TAG = RouteDialog.class.getName();

    static RouteDialogCallbacks mListener;
    AutoCompleteTextView from;
    AutoCompleteTextView to;
    int fromPid = -1;
    int toPid = -1;
    Button search;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (RouteDialogCallbacks) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_route, null);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_1,
                null,
                new String[]{SearchManager.SUGGEST_COLUMN_TEXT_1},
                new int[]{android.R.id.text1},
                0);

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                Uri uri = Uri.parse(LmnProvider.URI);
                return getActivity().getContentResolver().query(uri, null, " name LIKE ?", new String[]{charSequence.toString()}, null);
            }
        });

        adapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                int index = cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1);
                return cursor.getString(index);
            }
        });

        from = (AutoCompleteTextView) view.findViewById(R.id.from);
        from.setAdapter(adapter);
        from.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TODO : correct this cast
                fromPid = (int) (long) l;
            }
        });

        to = (AutoCompleteTextView) view.findViewById(R.id.to);
        to.setAdapter(adapter);
        to.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TODO : correct this cast
                toPid = (int) (long) l;
            }
        });

        ImageButton swapButton = (ImageButton) view.findViewById(R.id.swap);
        final View divider = view.findViewById(R.id.divider);
        swapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String toText = to.getText().toString();
                String fromText = from.getText().toString();

                to.setText(fromText);
                from.setText(toText);

                int tempFromPid = fromPid;
                fromPid = toPid;
                toPid = tempFromPid;

                //Remove focus to prevent suggestions re-appearing.
                from.clearFocus();
                divider.requestFocus();
            }
        });

        Button cancel = (Button) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        search = (Button) view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mListener.onDialogPositiveClick(from.getText().toString(), to.getText().toString());
                String[] poiNames = {from.getText().toString(), to.getText().toString()};
                int[] pids = {fromPid, toPid};
                mListener.onDialogPositiveClick1(poiNames, pids);
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        return builder.create();
    }

    public interface RouteDialogCallbacks {
        public void onDialogPositiveClick1(String[] poiNames, int[] pids);
    }
}
