package sg.helloworld.lmn.lastmile.main;

import android.app.DialogFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.qozix.tileview.TileView;
import com.qozix.tileview.paths.DrawablePath;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sg.helloworld.lmn.lastmile.R;
import sg.helloworld.lmn.lastmile.dialogs.RouteDialog;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterest;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterestWrapper;
import sg.helloworld.lmn.lastmile.models.route.RouteFeature;
import sg.helloworld.lmn.lastmile.models.route.RouteWrapper;
import sg.helloworld.lmn.lastmile.utils.ActionBarUtil;
import sg.helloworld.lmn.lastmile.utils.CoordinateConverter;
import sg.helloworld.lmn.lastmile.utils.LmnApiWrapper;
import sg.helloworld.lmn.lastmile.utils.PidLookupTaskFragment;
import sg.helloworld.lmn.lastmile.utils.TileViewActivity;

public class LocalTileActivity extends TileViewActivity implements RouteDialog.RouteDialogCallbacks, PidLookupTaskFragment.PidLookUpCallbacks {

    //Debug TAG
    private static final String TAG = LocalTileActivity.class.getName();

    //Constants for PidLookupTaskFragment
    public static final String MAP_NAME = "map_name";
    public static final String POI_NAMES = "poi_names";
    public static final String PID_TASK_TYPE = "pid_task_type";
    public static final int SOURCE_AND_TARGET = 0;
    public static final int SOURCE_ONLY = 1;
    public static final int TARGET_ONLY = 2;

    //Fragment id
    private static final String PID_LOOKUP_FRAGMENT_TAG = "pid_lookup_fragment_tag";

    //TODO : This variable is to be set dynamically from the parent activity (not yet implemented)
    private static final String mapName = "sggbtb";

    private static PidLookupTaskFragment pidLookupTaskFragment;
    LmnApiWrapper lmnService;
    TileView tileView;
    MenuItem item;

    //References to overlay items so that it can be cleared when needed
    View fromMarker;
    View toMarker;
    List<DrawablePath> paths;

    //TODO : Bad pid management, think of another way to do this
    int _fromPid = -1;
    int _toPid = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBarUtil.setupRobotoActionBar(this);

        lmnService = LmnApiWrapper.getInstance();

        tileView = getTileView();

        tileView.setSize(4096, 4096);

        tileView.addDetailLevel(1.0f, "gbtb_tiles/4/%col%/%row%.png", "gbtb_tiles/gbtb.png", 256, 256);
        tileView.addDetailLevel(0.5f, "gbtb_tiles/3/%col%/%row%.png", "gbtb_tiles/gbtb.png", 256, 256);
        tileView.addDetailLevel(0.25f, "gbtb_tiles/2/%col%/%row%.png", "gbtb_tiles/gbtb.png", 256, 256);
        tileView.addDetailLevel(0.125f, "gbtb_tiles/1/%col%/%row%.png", "gbtb_tiles/gbtb.png", 256, 256);

        tileView.setMarkerAnchorPoints(-0.5f, -0.9f);

        tileView.setScaleLimits(0.0, 2.0);
        tileView.setScale(0.5);

        frameTo(2048, 2048);

    }

    @Override
    protected void onNewIntent(Intent intent) {

        final String poiName;

        if (intent.getAction().equals(Intent.ACTION_VIEW)) {
            //User selected search suggestion
            poiName = intent.getDataString();
            Log.d(TAG, "poiName = " + poiName);

            lmnService.getPoi(mapName, poiName, new Callback<PointOfInterestWrapper>() {
                @Override
                public void success(PointOfInterestWrapper pointOfInterestWrapper, Response response) {
                    showPoiOnMap(pointOfInterestWrapper.getPois().get(0));
                    //Collapse the action bar search view since we're done with it.
                    if (item != null) item.collapseActionView();
                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                }
            });
        } else if (intent.getAction().equals(Intent.ACTION_SEARCH)) {
            //User typed the entire query and hit enter
            poiName = intent.getStringExtra(SearchManager.QUERY);
            Log.d(TAG, "poiName = " + poiName);

            lmnService.getPoi(mapName, poiName, new Callback<PointOfInterestWrapper>() {
                @Override
                public void success(PointOfInterestWrapper pointOfInterestWrapper, Response response) {

                    if(pointOfInterestWrapper.getPois().size() < 1) {
                        //No pois were found for this query
                        Toast.makeText(LocalTileActivity.this, "No results for " + poiName, Toast.LENGTH_LONG).show();
                    } else if (pointOfInterestWrapper.getPois().size() == 1) {
                        //There is exactly one poi match for this query
                        showPoiOnMap(pointOfInterestWrapper.getPois().get(0));
                        //Collapse the action bar search view since we're done with it.
                        if (item != null) item.collapseActionView();
                    } else {
                        //there is more than one poi match for this poi query
                        //TODO: implement popup for user to chose the intended poi
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tile, menu);

        // Associate searchable configuration with the SearchView
        item = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_directions) {
            DialogFragment routeDialog = new RouteDialog();
            routeDialog.show(getFragmentManager(), "route");
        }
        return super.onOptionsItemSelected(item);
    }

    //Return from route dialog
    @Override
    public void onDialogPositiveClick1(String[] poiNames, int[] pids) {
        String from = poiNames[0];
        String to = poiNames[1];

        int fromPid = pids[0];
        int toPid = pids[1];

        if (fromPid > -1 && toPid > -1) {
            //Both pids are valid, just draw the route
            getRoute(mapName, fromPid, toPid);
            return;
        } else if (fromPid <= -1 && toPid > -1) {
            //from needs to be evaluated
            _toPid = toPid;
            if (pidLookupTaskFragment != null) {
                getFragmentManager().beginTransaction().remove(pidLookupTaskFragment).commitAllowingStateLoss();
                pidLookupTaskFragment = null;
            }

            pidLookupTaskFragment = new PidLookupTaskFragment();

            Bundle bundle = new Bundle();
            bundle.putString(MAP_NAME, mapName);
            bundle.putInt(PID_TASK_TYPE, SOURCE_ONLY);
            bundle.putStringArray(POI_NAMES, new String[]{from, to});
            pidLookupTaskFragment.setArguments(bundle);

            getFragmentManager().beginTransaction().add(pidLookupTaskFragment, PID_LOOKUP_FRAGMENT_TAG).commit();
            return;
        } else if (fromPid > -1 && toPid <= -1) {
            //to needs to be evaluated
            _fromPid = fromPid;
            if (pidLookupTaskFragment != null) {
                getFragmentManager().beginTransaction().remove(pidLookupTaskFragment).commitAllowingStateLoss();
                pidLookupTaskFragment = null;
            }

            pidLookupTaskFragment = new PidLookupTaskFragment();

            Bundle bundle = new Bundle();
            bundle.putString(MAP_NAME, mapName);
            bundle.putInt(PID_TASK_TYPE, TARGET_ONLY);
            bundle.putStringArray(POI_NAMES, new String[]{from, to});
            pidLookupTaskFragment.setArguments(bundle);

            getFragmentManager().beginTransaction().add(pidLookupTaskFragment, PID_LOOKUP_FRAGMENT_TAG).commit();
            return;
        } else {
            //both from and to need to be evaluated
            if (pidLookupTaskFragment != null) {
                getFragmentManager().beginTransaction().remove(pidLookupTaskFragment).commitAllowingStateLoss();
                pidLookupTaskFragment = null;
            }

            pidLookupTaskFragment = new PidLookupTaskFragment();

            Bundle bundle = new Bundle();
            bundle.putString(MAP_NAME, mapName);
            bundle.putInt(PID_TASK_TYPE, SOURCE_AND_TARGET);
            bundle.putStringArray(POI_NAMES, new String[]{from, to});
            pidLookupTaskFragment.setArguments(bundle);

            getFragmentManager().beginTransaction().add(pidLookupTaskFragment, PID_LOOKUP_FRAGMENT_TAG).commit();
            return;
        }
    }

    @Override
    public void onPreExecute() {
    }

    @Override
    public void onProgressUpdate() {

    }

    @Override
    public void onCancelled(String reason) {

        Toast.makeText(LocalTileActivity.this, reason, Toast.LENGTH_LONG).show();

        getFragmentManager().beginTransaction().remove(pidLookupTaskFragment).commitAllowingStateLoss();
        pidLookupTaskFragment = null;

        if (fromMarker != null) tileView.removeMarker(fromMarker);
        if (toMarker != null) tileView.removeMarker(toMarker);
        if (paths != null) {
            for (DrawablePath path : paths) {
                tileView.removePath(path);
            }
        }

        _fromPid = -1;
        _toPid = -1;
    }

    @Override
    public void onPostExecute(int taskType, String[] poiNames, List<Integer[]> pidList) {

        int fromPid = -1;
        int toPid = -1;

        getFragmentManager().beginTransaction().remove(pidLookupTaskFragment).commitAllowingStateLoss();
        pidLookupTaskFragment = null;

        switch (taskType) {
            case SOURCE_AND_TARGET:
                //get pid for source
                if (pidList.get(0).length > 1) {
                    //more than one pid has returned for this source
                    //TODO : popup dialog to select the correct pid
                    Toast.makeText(LocalTileActivity.this, "Multiple source/target has not been implemented yet.", Toast.LENGTH_LONG).show();
                } else fromPid = pidList.get(0)[0];

                //get pid for target
                if (pidList.get(1).length > 1) {
                    //more than one pid has returned for this source
                    //TODO : popup dialog to select the correct pid
                    Toast.makeText(LocalTileActivity.this, "Multiple source/target has not been implemented yet.", Toast.LENGTH_LONG).show();
                } else toPid = pidList.get(1)[0];
                getRoute(mapName, fromPid, toPid);
                break;
            case SOURCE_ONLY:
                //get pid for source
                if (pidList.get(0).length > 1) {
                    //more than one pid has returned for this source
                    //TODO : popup dialog to select the correct pid
                    Toast.makeText(LocalTileActivity.this, "Multiple source/target has not been implemented yet.", Toast.LENGTH_LONG).show();
                } else fromPid = pidList.get(0)[0];
                toPid = _toPid;
                getRoute(mapName, fromPid, toPid);
                break;
            case TARGET_ONLY:
                fromPid = _fromPid;
                //get pid for target
                if (pidList.get(1).length > 1) {
                    //more than one pid has returned for this source
                    //TODO : popup dialog to select the correct pid
                    Toast.makeText(LocalTileActivity.this, "Multiple source/target has not been implemented yet.", Toast.LENGTH_LONG).show();
                } else toPid = pidList.get(1)[0];
                getRoute(mapName, fromPid, toPid);
                break;
            default:
                Toast.makeText(LocalTileActivity.this, "Unknown error while routing.", Toast.LENGTH_LONG).show();
                break;
        }

        _fromPid = -1;
        _toPid = -1;

    }

    //Helper method get route from lmn service and draw it on map
    private void getRoute(String mapName, int source, int target) {
        lmnService.getRoute(mapName, source, target, new Callback<RouteWrapper>() {
            @Override
            public void success(RouteWrapper routeWrapper, Response response) {

                if (routeWrapper.getFeatures().size() < 1) {
                    Toast.makeText(LocalTileActivity.this, "Route could not be found", Toast.LENGTH_LONG).show();
                } else {
                    showRouteOnMap(routeWrapper);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    //Helper method to draw route on map
    private void showRouteOnMap(RouteWrapper routeWrapper) {
        //clear the map first
        clearRoute();

        //add start and end markers
        double[] source = CoordinateConverter.getConvertedDouble(routeWrapper.
                getProperties().
                get(0).
                getSource().
                getCoordinates());

        fromMarker = addPin(source[0], source[1]);

        double[] target = CoordinateConverter.getConvertedDouble(routeWrapper.
                getProperties().
                get(1).
                getTarget().
                getCoordinates());

        toMarker = addPin(target[0], target[1]);

        //draw the path
        Paint paint = tileView.getPathPaint();
        paint.setColor(getResources().getColor(android.R.color.holo_red_light));
        paint.setAlpha(205);
        paint.setStrokeWidth(10.f);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setAntiAlias(true);

        paths = new ArrayList<DrawablePath>();

        List<double[]> totalPoints = new ArrayList<double[]>();
        for (RouteFeature feature : routeWrapper.getFeatures()) {
            List<double[]> points = CoordinateConverter.getConvertedDoubles(feature.
                    getGeometry().
                    getCoordinates());
            DrawablePath path = tileView.drawPath(points);
            paths.add(path);

            for (double[] point : points) {
                totalPoints.add(point);
            }
        }

        smoothFramePoints(totalPoints, 0.8);
    }

    //Helper method to show poi on map
    private void showPoiOnMap(PointOfInterest poi) {
        //Clear the map first
        clearRoute();

        //Need to convert to TileViews coordinate format
        double[] point = CoordinateConverter.getConvertedDouble(poi.getGeometry().getCoordinates());
        tileView.slideToAndCenter(point[0], point[1]);
        tileView.postDelayed(new Runnable() {
            @Override
            public void run() {
                tileView.smoothScaleTo(1.5, 1000);
            }
        }, 500);
    }

    //Helper method to clear route
    private void clearRoute() {
        if (fromMarker != null) tileView.removeMarker(fromMarker);
        if (toMarker != null) tileView.removeMarker(toMarker);
        if (paths != null) {
            for (DrawablePath path : paths) {
                tileView.removePath(path);
            }
        }
    }

    //Helper method to add a marker on the map
    private View addPin(double x, double y) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.maps_marker_red);
        return getTileView().addMarker(imageView, x, y);
    }
}
