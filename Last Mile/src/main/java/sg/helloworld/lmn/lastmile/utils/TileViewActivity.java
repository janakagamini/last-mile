package sg.helloworld.lmn.lastmile.utils;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;

import com.qozix.tileview.TileView;

import java.util.List;

/**
 * Created by Janaka on 11/4/2014.
 */
public class TileViewActivity extends Activity {

    private TileView tileView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tileView = new TileView(this);
        setContentView(tileView);
    }

    @Override
    public void onPause() {
        super.onPause();
        tileView.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        tileView.resume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tileView.destroy();
        tileView = null;
    }

    public TileView getTileView() {
        return tileView;
    }

    /**
     * This is a convenience method to moveToAndCenter after layout (which won't happen if called directly in onCreate
     * see https://github.com/moagrius/TileView/wiki/FAQ
     */
    public void frameTo(final double x, final double y) {
        getTileView().post(new Runnable() {
            @Override
            public void run() {
                getTileView().moveToAndCenter(x, y);
            }
        });
    }

    public void framePoints(final List<double[]> points) {
        getTileView().post(new Runnable() {
            @Override
            public void run() {
                getTileView().framePoints(points);
            }
        });
    }

    public void smoothFramePoints(List<double[]> points, final double scalePercentage) {

        double topMost = -Integer.MAX_VALUE;
        double bottomMost = Integer.MAX_VALUE;
        double leftMost = Integer.MAX_VALUE;
        double rightMost = -Integer.MAX_VALUE;

        for (double[] coordinate : points) {
            double x = coordinate[0];
            double y = coordinate[1];
            if (getTileView().getPositionManager().contains(x, y)) {
                topMost = Math.max(topMost, x);
                bottomMost = Math.min(bottomMost, x);
                leftMost = Math.min(leftMost, y);
                rightMost = Math.max(rightMost, y);
            }
        }

        Point topRight = getTileView().getPositionManager().translate(topMost, rightMost);
        Point bottomLeft = getTileView().getPositionManager().translate(bottomMost, leftMost);

        int width = bottomLeft.x - topRight.x;
        int height = bottomLeft.y - topRight.y;

        double scaleX = Math.abs(getTileView().getWidth() / (double) width);
        double scaleY = Math.abs(getTileView().getHeight() / (double) height);

        final double destinationScale = Math.min(scaleX, scaleY);

        double middleX = (rightMost + leftMost) * 0.5f;
        double middleY = (topMost + bottomMost) * 0.5f;

        getTileView().slideToAndCenter(middleY, middleX);
        getTileView().postDelayed(new Runnable() {
            @Override
            public void run() {
                getTileView().smoothScaleTo(destinationScale * scalePercentage, 500);
            }
        }, 500);

    }
}