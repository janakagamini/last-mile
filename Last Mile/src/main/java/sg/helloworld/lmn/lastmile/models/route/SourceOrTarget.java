package sg.helloworld.lmn.lastmile.models.route;

/**
 * Created by Janaka on 14/4/2014.
 */
public class SourceOrTarget {
    private String type;
    private double[] coordinates;

    public double[] getCoordinates() {
        return coordinates;
    }

    public String getType() {
        return type;
    }
}
