package sg.helloworld.lmn.lastmile.models.route;

/**
 * Created by Janaka on 4/13/2014.
 */
public class RouteProperty {
    private SourceOrTarget source;
    private SourceOrTarget target;

    public SourceOrTarget getSource() {
        return source;
    }

    public SourceOrTarget getTarget() {
        return target;
    }
}
