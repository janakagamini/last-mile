package sg.helloworld.lmn.lastmile.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import sg.helloworld.lmn.lastmile.R;

/**
 * Created by Janaka on 11/4/2014.
 */
public class ActionBarUtil {

    public static void setupRobotoActionBar(Activity activity) {
        activity.getActionBar().setDisplayShowCustomEnabled(true);
        activity.getActionBar().setDisplayShowTitleEnabled(false);

        LayoutInflater inflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_title, null);

        //if you need to customize anything else about the text, do it here.
        //I'm using a custom TextView with a custom font in my layout xml so all I need to do is set title
        ((TextView) v.findViewById(R.id.title)).setText(activity.getTitle());

        //assign the view to the actionbar
        activity.getActionBar().setCustomView(v);
    }
}
