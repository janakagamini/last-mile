package sg.helloworld.lmn.lastmile.main;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.qozix.tileview.TileView;
import com.qozix.tileview.graphics.BitmapDecoderHttp;

import java.util.ArrayList;
import java.util.List;

import sg.helloworld.lmn.lastmile.R;
import sg.helloworld.lmn.lastmile.utils.TileViewActivity;

public class OnlineTileActivity extends TileViewActivity {

    TileView tileView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tileView = getTileView();

        tileView.setDecoder(new BitmapDecoderHttp());
        tileView.setDownsampleDecoder(new BitmapDecoderHttp());

        tileView.setSize(4096, 4096);

        tileView.addDetailLevel(1.0f, "http://lmn.helloworld.sg/tiles/sg_gbtb2/4/%col%/%row%.png", "http://lmn.helloworld.sg/tiles/sg_gbtb2/gbtb.png");
        tileView.addDetailLevel(0.5f, "http://lmn.helloworld.sg/tiles/sg_gbtb2/3/%col%/%row%.png", "http://lmn.helloworld.sg/tiles/sg_gbtb2/gbtb.png");
        tileView.addDetailLevel(0.25f, "http://lmn.helloworld.sg/tiles/sg_gbtb2/2/%col%/%row%.png", "http://lmn.helloworld.sg/tiles/sg_gbtb2/gbtb.png");
        tileView.addDetailLevel(0.125f, "http://lmn.helloworld.sg/tiles/sg_gbtb2/1/%col%/%row%.png", "http://lmn.helloworld.sg/tiles/sg_gbtb2/gbtb.png");

        tileView.setMarkerAnchorPoints(-0.5f, -0.5f);

        addPin(2203, 1583);
        addPin(2278, 1518);

        tileView.setScaleLimits(0.0, 2.0);
        //tileView.setScale(0.125);
        tileView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("DEBUG", "scale: " + tileView.getScale());
                return false;
            }
        });

        List<double[]> positions = new ArrayList<double[]>();
        positions.add(new double[]{2203, 1583});
        positions.add(new double[]{2278, 1518});

        tileView.drawPath(positions);

        //tileView.setScaleToFit(true);
        positions = new ArrayList<double[]>();
        positions.add(new double[]{0, 0});
        positions.add(new double[]{4096, 4096});
        tileView.framePoints(positions);

        // center the frame
        frameTo(1000, 1000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.online_tile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addPin(double x, double y) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.maps_marker_blue);
        getTileView().addMarker(imageView, x, y - 100);
    }

}
