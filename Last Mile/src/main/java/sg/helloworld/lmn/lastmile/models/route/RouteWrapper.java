package sg.helloworld.lmn.lastmile.models.route;

import java.util.List;

/**
 * Created by Janaka on 4/13/2014.
 */
public class RouteWrapper {

    private String type;
    private List<RouteFeature> features;
    private List<RouteProperty> properties;
    private CoordinateReferenceSystem crs;

    public String getType() {
        return type;
    }

    public List<RouteFeature> getFeatures() {
        return features;
    }

    public List<RouteProperty> getProperties() {
        return properties;
    }

    public CoordinateReferenceSystem getCrs() {
        return crs;
    }
}
