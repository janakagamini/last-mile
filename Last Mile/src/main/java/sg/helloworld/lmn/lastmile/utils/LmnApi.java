package sg.helloworld.lmn.lastmile.utils;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import sg.helloworld.lmn.lastmile.models.poi.PointOfInterestWrapper;
import sg.helloworld.lmn.lastmile.models.route.RouteWrapper;

/**
 * Created by Janaka on 4/13/2014.
 */
public interface LmnApi {

    /*http://lmn.helloworld.sg:3000/api?method={<mapName>+GET_POI}&code={poiName}*/
    @GET("/api")
    void getPoi(@Query("method") String mapName,
                @Query("code") String poiName,
                Callback<PointOfInterestWrapper> cb);

    /*GET Blocking*/
    /*http://lmn.helloworld.sg:3000/api?method={<mapName>+GET_POI}&code={poiName}*/
    @GET("/api")
    PointOfInterestWrapper getPoi(@Query("method") String mapName, @Query("code") String poiName);

    /*http://lmn.helloworld.sg:3000/api?method={<mapName>+<GET_ROUTE>}&source={source}&target={target}*/
    @GET("/api")
    void getRoute(@Query("method") String mapName,
                  @Query("source") int source,
                  @Query("target") int target,
                  Callback<RouteWrapper> cb);

}
