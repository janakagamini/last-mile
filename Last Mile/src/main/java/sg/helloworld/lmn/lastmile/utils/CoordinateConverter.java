package sg.helloworld.lmn.lastmile.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by janaka on 4/17/14.
 */
public class CoordinateConverter {

    public static double[] getConvertedDouble(double[] point) {
        double[] _point = new double[2];
        _point[0] = point[0];
        _point[1] = point[1] * -1;
        return _point;
    }

    public static List<double[]> getConvertedDoubles(List<double[]> points) {
        List<double[]> _points = new ArrayList<double[]>(points.size());

        for (double[] point : points) {
            _points.add(getConvertedDouble(point));
        }

        return _points;
    }
}
