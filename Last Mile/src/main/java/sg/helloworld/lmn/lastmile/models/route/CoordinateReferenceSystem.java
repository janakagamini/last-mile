package sg.helloworld.lmn.lastmile.models.route;

/**
 * Created by Janaka on 4/13/2014.
 */
public class CoordinateReferenceSystem {
    private String type;
    private CrsProperty properties;

    public String getType() {
        return type;
    }

    public CrsProperty getProperties() {
        return properties;
    }
}
